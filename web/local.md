---
layout: page
title: "Location + Travel"
permalink: /local/
---

## Tirana, Albania

LAS 2025 will take place in Tirana! This page provides more details about our location and venue, some limited guidance on where to eat and stay, some things you might want to check out if you have extra time, and logistical information.

---

## Table of Contents
**[About Tirana](#about)**  
**[Venue](#venue)**  
**[Getting To/From Tirana](#getting-there)**   
**[Where to Sleep/Food/Drink](#capitalism)**   

---

<div id="about"/>

## About Tirana
Tirana is the capital and largest city of Albania. It is located in the center of the country, enclosed by mountains and hills, with Dajti rising to the east and a slight valley to the northwest overlooking the Adriatic Sea in the distance. It is among the wettest and sunniest cities in Europe, with 2,544 hours of sun per year. Around 1 million people are currently living in Tirana.


<div id="venue"/>

## The Venue

The venue is called : Ambasada e Paqes (in English: the Embassy of Peace).

Address:

    Ambasada e Paqes
    8R6P+4W7
    Rruga Xibrakeve
    Tiranë, Albani

[[Google Maps link](https://maps.app.goo.gl/wwnUscDJ3ELKcmo47)] | [[Open Street Maps link](https://www.openstreetmap.org/?mlat=41.310383&mlon=19.837183#map=19/41.310383/19.837183)]

![aerial view of the venue in Tirana](/assets/tirana.png)

<div id="getting-there"/>

## Getting to Tirana

### Visa restrictions
These are general guidelines, please double-check with your local Mexican consulate if there are different requirements for you.

#### Check if you need a VISA


You won't need a visa if you are from the **EU**, **Canada**, **US**, or **UK**. But for a full list of allowed entrances without a visa, please see [Who can enter Albania without a VISA](https://ambasadat.gov.al/japan/sites/default/files/Who_can_enter_Albania_without_a_Visa-word_0.pdf).

#### Asking for an LAS Invitation Letter
Please register and indicate on your form that you need a visa letter. You can also contact us at info@linuxappsummit.org to request a letter.

### By Plane  
Tirana is very easy to reach from any country in Europe and beyond. Its proximity to Turkey facilitates connections with Asian countries, while its closeness to Italy makes it convenient for the Western hemisphere. Many low-cost airlines operate at Tirana International Airport (TIA), making travel more financially friendly.

### By Train
Unfortunately, no commercial trains are operating to Tirana.

### By Bus
Germany, Italy, Austria, Greece, and a few other countries offer bus services to Albania. The journey can take up to 48-72 hours.

### By Ferry
There are ferries from Bari, Italy, which run several times a day from our port city, Durrës, located just a 45-minute drive from Tirana. This is a popular transportation method for both tourists and locals who wish to bring their cars. 

## Transportation to the venue

### Taxi 
The most convenient way to reach the venue is by taxi. 
You can [install the BlueTaxi app from the Google play store](https://play.google.com/store/apps/details?id=com.netinformatika.bluetaxipristina). (There is no BlueTaxi app on F-Droid.)
BlueTaxi works the same way that Uber does. 


Or call these companies in these phone numbers: 
* Blue Taxi: +355 69 444 4444
* Green Taxi: Toll Free: 0800 2000 - (you can book via WhatsApp as well)
* LuxTaxi: +355 4 562 0333 - You can also install their app in [Google Play](https://play.google.com/store/apps/details?id=taxi.lux.taxi&hl=en_US&pli=1) or [Apple Store](https://apps.apple.com/us/app/taxi-lux/id6446114257). (There is no Taxi LUX app on F-Droid).

At the destination please add: AMBASADA E PAQES

### Bus 
[Download a pdf bus stop map](https://transformative-mobility.org/wp-content/uploads/2023/05/Urbani_Digital_Map_ALB-edge-to-edge-25.07.2024.pdf)

Take the bus called Sauk/Vilat Gjermane. 
In the city center go to the station called Dollari 8A. 
It's located right on the side of Central Bank of Albania 
From there you will have 7 bus stops and you have to get off at: Pallati Brigadave. 

<div id="capitalism"/>

## Where to Stay

#### Tirana Mariott hotel 
- Various room options that start at about 160 EUR/night
- Can get breakfast for an upcharge
- https://www.marriott.com/en-us/hotels/tiamc-tirana-marriott/overview/?scid=f2ae0541-1279-4f24-b197-a979c79310b0 
- 6 min drive or a 28 min walk to the venue

#### Rogner Tirana
- Various room options that start at about 120 EUR/night
- Includes breakfast
- https://www.hotel-europapark.com/
- 7 min drive or a 33 min walk to the venue

#### Maritim Hotel Plaza Tirana 
- Various room options that start at about 180 EUR/night
- Can get breakfast for an upcharge
- https://www.plazatirana.com/
- 13 min drive or a 40 min walk to the venue

#### Freddys hotel 
- Various room options that start at about 70 EUR/night
- Includes breakfast
- Short url: https://shorturl.at/sebd7
- 18 min drive or a 48 min walk to the venue 
